package com.zyinhao.entity;

import java.util.Date;

/**
 * Created by zhangyinhao on 2018/8/10
 * Description：
 */
public class User {
    private String id;
    private String name;
    private String code;
    private Integer age;
    private Date createTime;

    public User() {
    }

    public User(String id, String name, String code, Integer age, Date createTime) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.age = age;
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", age=" + age +
                ", createTime=" + createTime +
                '}';
    }
}
