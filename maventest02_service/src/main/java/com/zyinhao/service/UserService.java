package com.zyinhao.service;

import com.zyinhao.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangyinhao on 2018/8/10
 * Description：
 */
public interface UserService {

    /**
     * 查
     * @param params
     * @return
     */
    public List<User> find(Map<String,Object> params);
    /**
     * 增
     * @param user
     */
    public void save(User user);
    /**
     * 改
     * @param user
     */
    public void update(User user);
    /**
     * 删
     * @param ids
     */
    public void delete(String ids);
}
