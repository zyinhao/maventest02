package com.zyinhao.service.impl;

import com.zyinhao.entity.User;
import com.zyinhao.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by zhangyinhao on 2018/8/10
 * Description：
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<User> find(Map<String,Object> params) {
        System.out.println("####service:user:find");
        return null;
    }

    @Override
    public void save(User user) {
        System.out.println("####service:user:save");
    }

    @Override
    public void update(User user) {
        System.out.println("####service:user:update");
    }

    @Override
    public void delete(String ids) {
        System.out.println("####service:user:delete");
    }
}
