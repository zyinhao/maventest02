package com.zyinhao.constants;

public enum RespCodeConstants {
    
		SUCCESS																	("000000", "成功"), 

		/** 公共通用错误码 **/
		COMMON_PARAM_ERROR														("000001", "参数错误"), 
		COMMON_NOT_EXIST_ERROR													("000002", "数据不存在"), 
		COMMON_CONNECT_TIMEOUT_ERROR											("000003", "连接超时"), 
		COMMON_REMOTE_CALL_ERROR												("000004", "远程调用异常"), 
		COMMON_REMOTE_RET_NULL_ERROR											("000005", "远程调用返回空"), 
		COMMON_REMOTE_RET_FAIL_ERROR											("000006", "远程调用返回失败"), 
		COMMON_REQDATA_INVALID_ERROR											("000007", "请求数据非法"), 
		COMMON_REQIP_INVALID_ERROR												("000008", "请求IP非法"), 
		COMMON_DATA_CHECK_FAIL_ERROR											("000009", "请求数据校验失败"), 
		COMMON_DATA_REPEAT_ERROR												("000011", "数据重复"), 
		COMMON_IN_OBJECT_NULL_ERROR												("000012", "传入对象不能为空"), 
		COMMON_MUST_PARAM_NULL_ERROR											("000013", "必要参数不能为空"), 
		COMMON_NO_PRIVILIEGS_ERROR												("000014", "权限不足"), 
		COMMON_DATABASE_ERROR													("000015", "数据库异常"), 
		COMMON_SYSTEM_TIMEOUT_ERROR												("000016", "系统超时"), 
		COMMON_SYSTEM_ERROR														("000099", "系统错误"), 

		/** 运营配置错误码 **/
		WEB_USER_NAME_ERROR														("010001", "用户名错误"), 
		WEB_PASSWORD_ERROR														("010002", "密码错误"), 
		WEB_VERIFY_CODE_ERROR													("010003", "验证码错误"), 
		WEB_USER_NOT_EXISTS_ERROR											    ("010004", "系统用户不存在"), 
		WEB_REQ_CMS_FILE_SERVICE_ERROR											("010201", "请求CMS文件服务失败"), 

		/** 客户端接口错误码 **/
		API_USER_NAME_NOT_EXISTS_ERROR											("020001", "登录用户名不存在"), 
		API_USER_PASSWORD_ERROR													("020002", "登录密码不正确"), 
		API_TOKEN_NOT_VALID_ERROR												("020003", "TOKEN失效"), 
		API_USER_COLLECT_FALIE_ERROR											("020004", "用户收藏同步失败"), 
		API_USER_REGISTER_FAIL_ERROR											("020005", "用户注册失败"), 
		API_USER_LOGIN_FAIL_ERROR												("020006", "用户登录失败"), 
		API_USER_LOGOUT_FAIL_ERROR												("020007", "用户退出失败"), 
		API_USER_COLLECT_QUERY_FAIL_ERROR										("020008", "用户收藏查询失败"), 
		API_USER_REGISTED_ERROR													("020009", "用户已经注册"), 
		API_USER_NOT_EXISTS_ERROR                                               ("020010", "用户不存在"), 
		API_CAR_LIST_QUERY_FAIL_ERROR											("020101", "车辆列表查询失败"), 

		/** 文件错误码 **/
		FILE_STORAGE_FAILE_ERROR												("030001", "文件存储失败"), 
		FILE_DELETE_FAILE_ERROR													("030002", "文件删除失败"), 
		FILE_DOWNLOAD_FAILE_ERROR												("030003", "文件下载失败");

		private String value;
    
	    private String chName;
	
	    private RespCodeConstants(String value, String chName) {
	        this.value = value;
	        this.chName = chName;
	    }
	
	    public String getCode() {
	        return value;
	    }
	    
	    public String getName(){
	        return chName;
	    }

}
