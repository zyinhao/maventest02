<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>maventest02</title>
</head>
<body>
<h2>Hello World!1</h2>
<p>restful接口测试：<br>
    window.fetch('http://localhost:9002/user').then(response => response.json()).then(data => console.log(data))<br>
    window.fetch('http://localhost:9002/user',{method: 'POST',headers: { 'Content-Type': 'application/json;charset=UTF-8' },body: JSON.stringify({'name':'aaa','code':'bbb'})}).then((response) => {return response.json();}).then(data => console.log(data))<br>
    window.fetch('http://localhost:9002/user/1',{method: 'PUT',headers: { 'Content-Type': 'application/json;charset=UTF-8' },body: JSON.stringify({'name':'update1'})}).then((response) => {return response.json();}).then(data => console.log(data))<br>
    window.fetch('http://localhost:9002/user/1',{method: 'delete'}).then((response) => {return response.json();}).then(data => console.log(data))<br>
</p>
<p>swagger api文档：http://localhost:9002/swagger-ui.html<br>
    swagger ui 根据json生成：http://localhost:9002/v2/api-docs<br>
</p>
</body>
</html>
