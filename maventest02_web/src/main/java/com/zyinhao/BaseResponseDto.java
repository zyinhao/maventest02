package com.zyinhao;

import com.zyinhao.constants.RespCodeConstants;

import java.io.Serializable;

public class BaseResponseDto implements Serializable{
    private static final long serialVersionUID = -1069173771761032587L;
    private String code = RespCodeConstants.SUCCESS.getCode();
    private String desc = RespCodeConstants.SUCCESS.getName();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
