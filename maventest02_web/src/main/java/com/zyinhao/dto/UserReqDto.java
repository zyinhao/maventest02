package com.zyinhao.dto;

import com.zyinhao.BaseRequestDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zhangyinhao on 2018/8/9
 * Description：
 */
@ApiModel(value="用户查询对象模型")
public class UserReqDto extends BaseRequestDto {
    @ApiModelProperty(value="id" ,required=false)
    private String id;
    @ApiModelProperty(value="用户名称" ,required=false)
    private String name;
    @ApiModelProperty(value="用户编码" ,required=false)
    private String code;
    @ApiModelProperty(value="用户年龄" ,required=false)
    private Integer age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserReqDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", age=" + age +
                '}';
    }
}
