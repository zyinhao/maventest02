package com.zyinhao.vo;

import com.zyinhao.constants.RespCodeConstants;

public class JsonResults<T> {
	private String code = RespCodeConstants.SUCCESS.getCode();
	private String desc = RespCodeConstants.SUCCESS.getName();
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
