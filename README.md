### 项目名称：maventest02
### 项目描述：使用maven搭建父子项目
### 功能点：maven父子项目，通过父项目的pom.xml方便控制多个子项目：比如：依赖包版本、JDK版本、插件使用等共同属性；

## 一：环境
    JDK：1.8
	开发工具：idea
	maven：3.5

## 二：maven基本概念
>* [参考test01项目：maven构建web项目](https://gitee.com/zyinhao/test01)：https://gitee.com/zyinhao/test01
>* 超级POM：所有的POM继承自父类,默认不可见；maven提供一个基础pom.xml,所有pom都继承于此，因此默认配置不用写；
>* 父子生命周期：Lifecycle：运行父项目生命阶段命令会级联运行所有子项目的对应的生命阶段；子项目生命阶段只针对自身；

## 三：创建父子项目
### 1:创建父项目
>* 项目三要素：com.zyinhao:maventest02:1.0-SNAPSHOT
>* 删除src目录：父目录只需要维护pom.xml，供子项目继承就可以了
>* packaging：打包类型为pom，当新增子项目的时候会自动补充在pom.xml

### 2:增加子项目:web、service、util
>* module：File->New->Module
>* archetype选择
>>* archetype：web：org.apache.maven.archetypes:maven-archetype-webapp
>>* archetype：service、util：org.apache.maven.archetypes:maven-archetype-quickstart
>>* archetype大全参考：https://blog.csdn.net/cx1110162/article/details/78297654

### 3:将子pom.xml共同配置，提取到父pom.xml，并删除对应子项目POM的配置，不然会被覆盖
>* properties：属性：JDK、依赖版本
>* dependencies->dependency：默认所有子项目的依赖
>* dependencyManagement->dependencies：预制可能使用的依赖，子项目使用依赖不用指定版本，从而达到统一版本管理
>* 命令以及插件配置：此处使用默认配置
>* 测试是否存在问题：Maven Project(右侧视图)->maventest02->Lifecycle->install

### 4:完善子项目依赖
![完善子项目依赖](https://gitee.com/zyinhao/maventest02/raw/master/images/4sub_project_dependency.jpg)


### 5:运行项目
>* [参考test01项目：配置tomcat，运行项目](https://gitee.com/zyinhao/test01)
>* 

### 6:增加restful访问接口：详见：UserController.java
>* GET：查询；POST：新增；PUT：修改；DELETE：删除；前面路径都是一样，区别与请求方法
>* DTO(Data Transfer Object)：数据传输对象；VO(View Object)：视图对象；两者用于前后台交互数据的封装，简单的时候也可以直接用entity，但是entity一般与数据库表的映射;
>* DTO可以用于系统与前台、数据库数据传输的封装；当然，如果数据比较简单，可以直接使用entity来接收数据；DTO可用于接收包含entity之外的数据；
>* 通过浏览器测试接口：GET：window.fetch('http://localhost:9002/user').then(response => response.json()).then(data => console.log(data))
>* 通过浏览器测试接口：POST：window.fetch('http://localhost:9002/user',{method: 'POST',headers: { 'Content-Type': 'application/json;charset=UTF-8' },body: JSON.stringify({'name':'aaa','code':'bbb'})}).then((response) => {return response.json();}).then(data => console.log(data))
>* 跨域说明
>>* 跨域指请求和服务的域不一致，浏览器和H5的ajax请求有影响，而对服务端之间的http请求没有限制。
>>* 跨域是浏览器拦截了服务器端返回的相应，不是拦截了请求。
>>* 解决方法：通过拦截器设置header，应用允许跨域：HeaderFilter.java
>* ![增加restful访问接口](https://gitee.com/zyinhao/maventest02/raw/master/images/6restful_code.jpg)

### 7:Swagger API文档:Swagger2.java
>* 访问地址：http://项目实际地址/swagger-ui.html
>* 参考文章：https://blog.csdn.net/sanyaoxu_2/article/details/80555328
>* Swagger官网 ：http://swagger.io/
>* Spring Boot & Swagger UI ： http://fruzenshtein.com/spring-boot-swagger-ui/
>* Github：https://github.com/swagger-api/swagger-core/wiki/Annotations
>* ![Swagger API文档](https://gitee.com/zyinhao/maventest02/raw/master/images/7swagger api.jpg)

## 四：maven高级知识

## 五：参考
* [参考test01项目：maven构建web项目](https://gitee.com/zyinhao/test01)：https://gitee.com/zyinhao/test01
* [maven教程](https://www.yiibai.com/maven/):易百教程，比较详细
* [官网](http://maven.apache.org/)：软件下载、教程
* [官网pom.xml解析](http://maven.apache.org/pom.html)：pom.xml组成要素解析
* [如何Spring Cloud Zuul作为网关的分布式系统中整合Swagger文档在同一个页面上](https://blog.csdn.net/qq6492178/article/details/78863935)：如何Spring Cloud Zuul作为网关的分布式系统中整合Swagger文档在同一个页面上